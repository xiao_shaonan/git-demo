package org.example;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * ClassName: SpringMVCController
 * Function : TODO
 *
 * @Author : shaonan.xiao
 * @Date : 2021/5/9 9:15
 **/
@RequestMapping("/v1")
@RestController
public class SpringMVCController {

    @GetMapping("/say")
    public String say(){
        return "hello";
    }

    @GetMapping("/say1")
    public String say1(){
        return "hello1";
    }

    @GetMapping("/say2")
    public String say2(){
        return "hello2";
    }
}
